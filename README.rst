========
PathDict
========

Python utility for working with nested data structures.

.. code:: python
            from pathdict import (PathDict, PathNotExistsError)

            d = PathDict(one=1, two=2, three=3)
            o = d.get('four.fff', 4, retpath=True)
            print(o.path)
            print(o.value)

            print()

            d = PathDict(one=1, two=2, three=3)
            o = d.get('one', retpath=True)
            print(o.path)
            print(o.value)

            print()

            d = PathDict({'one': {'two': {'three': 3}}})
            o = d.get('one.two.three', retpath=True)
            print(o.path)
            print(d.one.two.three)
            print(o.value)

            print()

            try:
                d = PathDict(one=1, two=2, three=3)
                o = d.get('five', 5, retpath=True, raise_error=True)
                d.raise_for_get()
            except PathNotExistsError as e:
                print(type(e))

            print()

            d = PathDict()
            d.set('one.two.three', [1, 2, 3]).set('one.two.three.4', 5, default=4).append('one.two.three.*', 6)
            print(d)

            print()

            d.delete('one.two.three.5')
            print(d)

            print()

            del d.one.two.three[4]
            print(d)


