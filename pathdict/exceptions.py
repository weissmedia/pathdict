from .utils import SENTINEL


class PathNotExistsError(Exception):
    def __init__(self, path: tuple=None, error_type=None, *args) -> None:
        self._path = path if isinstance(path, (list, tuple)) else [path or '']
        self._error_type = error_type or self.__class__
        super().__init__(self.message, *args)

    @property
    def message(self):
        error_type = {
            IndexError or KeyError or PathNotExistsError: 'Path "{}" not exists.',
            TypeError: 'Path "{}" is not been list or tuple.',
            SENTINEL: 'Default value not been set.',
        }.get(self._error_type, '{}').format('.'.join(map(str, self._path or [])))
        return error_type


class DuplicateComparisonTypeError(Exception):
    pass


class RequiredPathError(Exception):
    pass


class ComparedTypeError(Exception):
    pass
