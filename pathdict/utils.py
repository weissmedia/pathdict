from enum import IntEnum
from operator import ior
from functools import reduce


ASTERISK = '*'
SENTINEL = object()
CONDITIONS = dict()  # type: dict
REQUIRED_PATHS = []  # type: list


class ComparisonType(IntEnum):
    EQUAL = 1 << 0
    CHANGE = 1 << 1
    NEW = 1 << 2
    DELETE = 1 << 3
    REQUIRED = 1 << 4
    IGNORE = 1 << 5
    LEFT_CHANGE = 1 << 6
    RIGHT_CHANGE = 1 << 7
    equal = 1 << 8
    change = 1 << 9
    new = 1 << 10
    delete = 1 << 11
    required = 1 << 12
    ignore = 1 << 13
    left_change = 1 << 14
    right_change = 1 << 15
    e = equal
    c = change
    n = new
    d = delete
    r = required
    i = ignore
    lc = left_change
    cr = change
    E = EQUAL
    C = CHANGE
    N = NEW
    D = DELETE
    R = REQUIRED
    I = IGNORE
    LC = LEFT_CHANGE
    RC = RIGHT_CHANGE
    all = equal | change | new | delete | required | ignore
    ALL = EQUAL | CHANGE | NEW | DELETE | REQUIRED | IGNORE
    left = equal | left_change | delete | required
    LEFT = EQUAL | LEFT_CHANGE | DELETE | REQUIRED
    right = equal | right_change | new | ignore | required
    RIGHT = EQUAL | RIGHT_CHANGE | NEW | IGNORE | REQUIRED

    @property
    def value(self):
        d = dict(EQUAL='__EQUAL__',
                 CHANGE='__CHANGE__',
                 LEFT_CHANGE='__CHANGE__',
                 RIGHT_CHANGE='__CHANGE__',
                 NEW='__NEW__',
                 DELETE='__DELETE__',
                 REQUIRED='__REQUIRED__',
                 IGNORE='__IGNORE__')

        values = list(d.values())
        d.update(dict(zip(map(str.lower, d.keys()), values)))
        d.update(dict(all=values))
        d.update(dict(ALL=values))
        d.update(dict(left=values))
        d.update(dict(LEFT=values))
        d.update(dict(right=values))
        d.update(dict(RIGHT=values))
        return d[self._name_]

    def is_visible(self):
        return self._name_[0].isupper()

    def __eq__(self, other):
        return self.value == other.value

    def __ne__(self, other):
        return self.value != other.value

    def __sub__(self, other):
        return reduce(ior, [ct for ct in ComparisonType if ct & (int(self) - int(other)) and ct < ComparisonType.ALL])

    def __add__(self, other):
        return int(self) + int(other)


def deephash(obj, natsort=True) -> int:
    def hashed(value):
        if isinstance(value, dict):
            value = hash(tuple(sorted(value.items())))
        elif isinstance(value, set):
            value = hash(tuple(sorted(value)))
        elif isinstance(value, list):
            if natsort:
                value = sorted(value)
            value = hash(tuple(value))
        else:
            value = hash(value)
        return value

    def wrapper(obj, natsort):
        if isinstance(obj, dict):
            o, items = {}, obj.items()
        elif isinstance(obj, (list, tuple, set)):
            o = set() if isinstance(obj, set) else []
            items = enumerate(obj)
        else:
            items, o = [], obj

        for key, value in items:
            if isinstance(value, (dict, list, tuple, set)):
                value = wrapper(value, natsort)
            value = hashed(value)

            if isinstance(o, dict):
                o.update({key: value})
            elif isinstance(o, set):
                o.add(value)
            elif isinstance(o, list):
                o.append(value)
            else:
                raise Exception()
        return o
    return hashed(wrapper(obj, natsort))
