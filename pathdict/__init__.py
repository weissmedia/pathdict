from operator import ior
from functools import (reduce, wraps)
from typing import (Dict, Tuple, List, TypeVar, Callable, Any, Iterator, Union)
from collections import (OrderedDict, deque)
from reprlib import recursive_repr
from copy import deepcopy

from .utils import (SENTINEL, ASTERISK, CONDITIONS, REQUIRED_PATHS, ComparisonType, deephash)
from .exceptions import (PathNotExistsError, DuplicateComparisonTypeError, RequiredPathError, ComparedTypeError)

import warnings

__all__ = ['PathDict', 'ComparisonType', 'deephash']


# Declare type variable
C = TypeVar('C', Callable, None, list)
Conditions = List[Tuple[str, C]]


# Decorators
# ------------------------------------------------------------------------------

def tostruct(func):
    @wraps(func)
    def wrapper(*args, **kwargs):

        if isinstance(args[1], str):
            args = list(args)
            args[1] = tuple([int(p) if p.isdigit() else p for p in args[1].split('.')])

        elif isinstance(args[1], list):
            args = list(args)
            args[1] = tuple(args[1])

        return func(*args, **kwargs)
    return wrapper


def tobit(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        args = list(args)

        if len(args) > 1:
            if isinstance(args[1], (str, list)):
                args[1] = reduce(ior, [ComparisonType[v] for v in args[1]])

            all_ = [ComparisonType.ALL, ComparisonType.all]
            chars = {ct.name[0] for ct in ComparisonType if ct not in all_ and ct & args[1]}

            if any(c for c in chars if c.islower() and c.upper() in chars):
                raise DuplicateComparisonTypeError()

        return func(*args, **kwargs)
    return wrapper


def pathcondition(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        condition = kwargs.get('condition')  # type: Any
        rpath = kwargs.get('rpath')  # type: bool
        queue = func(*args, **kwargs)  # type: deque

        if not isinstance(queue, deque) and not condition:
            return queue

        set_rpath = lambda o: o if rpath else o.value

        for _ in queue.copy():
            obj = queue.popleft()

            if callable(condition):
                if isinstance(obj.value, (list, dict)):
                    for p, v in obj.value.items() if isinstance(obj.value, dict) else enumerate(obj.value):
                        if condition(p, v):
                            queue.append(set_rpath(PathDictItem(Path(*obj.path + (p,)), v)))

                elif condition(obj.value):
                    queue.append(set_rpath(obj))

            elif obj.value in condition or obj.value == condition or isinstance(obj.value, PathDict) \
                    and all([obj.value.get(p, rpath=True).value in w for p, w in condition]):
                queue.append(set_rpath(obj))

            elif isinstance(obj.value, list) and isinstance(condition, (list, tuple)):
                for i, o in enumerate(obj.value):
                    try:
                        if all([o.get(p, rpath=True).value in w for p, w in condition]):
                            queue.append(set_rpath(PathDictItem(Path(*obj.path + (i,)), o)))
                    except AttributeError:
                        pass

        return list(queue)
    return wrapper


def comparator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        SKIP_CHANGED = '__SKIP__'
        result = PathDict()
        self = args[0].copy()  # type: PathDict
        other = args[1].copy()  # type: PathDict

        conditions = kwargs.get('conditions', self.get_conditions())

        if isinstance(conditions, dict) and not isinstance(conditions, PathDict):
            conditions = PathDict({Path(*p.split('.')): v for p, v in conditions.items()})  # type: dict

        required_paths = kwargs.get('required_paths', self.get_required_paths())  # type: list

        # Check required paths exists
        for p in required_paths:
            if self.get(p) is None:
                raise RequiredPathError(p)
            else:
                result.append(p, self.get(p))

        # Cache ComparisonType paths
        ct_paths = set()

        # Set result objects from other comparison type condition
        for other_obj in other.pathitems():
            if any(other_obj.path.islast(ct.value) for ct in ComparisonType if ct != ComparisonType.IGNORE):
                result.append(other_obj.path[:-1], PathDict({other_obj.path[-1]: other_obj.value}))
                ct_paths.add(other_obj.path[:-1])

        # Path mappings
        path_mapping = {}

        # Cache ignore objects
        ignore_lst = []  # type: List[Tuple[tuple, Any]]

        # Compare self with other (EQUAL, CHANGE or DELETE)
        for obj in self.pathitems():
            # Type check self/other
            if not (isinstance(obj.value, (list, tuple, set)) == isinstance(other.get(obj.path), (list, tuple, set)))\
                    and obj.value and other.get(obj.path):

                raise ComparedTypeError('{} != {}'.format(type(obj.value), type(other.get(obj.path))))

            # Required path check
            if obj.path.islast(ComparisonType.REQUIRED.value):
                ct_paths.add(obj.path)
                if obj.value != other.get(obj.path[:-1], raise_error=True):
                    raise ValueError(other.get(obj.path[:-1]))

            # Path exists in result
            if any(obj.path.issuperpath(i) for i in ct_paths):
                continue

            # Search for path conditions
            is_condition, is_condition_subpath, is_condition_rule_none, is_condition_path_any = [False]*4

            condition_rules = None

            if conditions:
                is_condition = any(obj.path.isequal(i) for i in conditions.keys())
                is_condition_path_any = any(p[-1] == ASTERISK and obj.path.isequal(p[:-1])
                                            for p, c in conditions.items())
                is_condition_subpath = any(obj.path.issuperpath(i) for i in conditions.keys())
                is_condition_rule_none = any(obj.path.isequal(p[:-1] if p[-1] == ASTERISK else p) and c is None
                                             for p, c in conditions.items())

                condition_rules = []
                for p, c in conditions.items():
                    if obj.path.isequal(p):
                        condition_rules = c

            # Check other item exists
            try:
                oval = other.get(obj.path.replace(path_mapping)[1], raise_error=True)
            except PathNotExistsError:
                oval = None  # type: Union[list, dict]
                if not any(obj.path.issuperpath(i) for i in ct_paths):
                    result.append(obj.path, PathDict({ComparisonType.DELETE.value: obj.value}))
                    ct_paths.add(obj.path)
                    continue

            # Skip path __IGNORE__
            if isinstance(oval, self.__class__) and oval.get(ComparisonType.IGNORE.value):
                index = -1
                for i, item in enumerate(other.get(obj.path[:-1])):
                    if isinstance(item, dict) and not item.get(ComparisonType.IGNORE.value):
                        index += 1
                        path_mapping.update({obj.path[:-1] + (index,): obj.path[:-1] + (i,)})
                        if obj.path[-1] == index:
                            oval = item
                    else:
                        if not ignore_lst or not any(p == obj.path for p, v in ignore_lst):
                            ignore_lst.append((obj.path, oval))

                if any(oval.get(ct.value) for ct in ComparisonType):
                    result.append(obj.path, PathDict({ComparisonType.DELETE.value: obj.value}))
                    ct_paths.add(obj.path)
                    continue

            if any(p[-1] == ASTERISK and obj.path.isequal(p) for p, c in conditions.items()) and condition_rules:
                warnings.warn(
                    UserWarning(
                        'Condition {} for the path "{}" has no impact'.format(str(condition_rules), obj.path)))

            # Compare list or tuple
            if isinstance(obj.value, (list, tuple)) and ((is_condition_path_any and is_condition_rule_none)
                                                         or (is_condition and not is_condition_rule_none)):

                lst = []

                for val in obj.value:
                    if any(val == i.get(ct.value) for ct in ComparisonType for i in lst):
                        continue

                    if val in oval:
                        lst.append(PathDict({ComparisonType.EQUAL.value: val}))

                    if val not in [ov.get(ct.value) if ov.get(ct.value) else ov
                                   for ov in oval for ct in ComparisonType]:

                        is_not_ct = not any(val.get(ct.value) for ct in ComparisonType)

                        for p, ov in enumerate(oval[:]):
                            is_not_ct_ = (not any(ov.get(ct.value) for ct in ComparisonType) and is_not_ct)
                            if condition_rules and val != ov and is_not_ct_ and \
                                    all(val.get(vv) == ov.get(vv) for vv in condition_rules):
                                oval[p] = PathDict({SKIP_CHANGED: ov})
                                lst.append(PathDict({ComparisonType.CHANGE.value: (val, ov)}))
                                break
                        else:
                            if is_not_ct:
                                lst.append(PathDict({ComparisonType.DELETE.value: val}))
                            else:
                                lst.append(val)

                for ov in oval:
                    if ov.get(SKIP_CHANGED):
                        continue

                    if ov not in obj.value:
                        fn = all if obj.value and len(condition_rules or []) > 1 else any

                        if not(fn(val.get(vv) == ov.get(vv) for val in obj.value for vv in condition_rules or [])):
                            if not any(ov.get(ct.value) for ct in ComparisonType):
                                ov = PathDict({ComparisonType.NEW.value: ov})

                            lst.append(ov)

                        elif ov.get(ComparisonType.IGNORE.value):
                            lst.append(ov)

                result.append(obj.path, lst)
                ct_paths.add(obj.path)

            # Equal item
            elif obj.value == oval and not any(obj.path.issuperpath(i) for i in ct_paths):
                result.append(obj.path, PathDict({ComparisonType.EQUAL.value: obj.value}))
                ct_paths.add(obj.path)

            # All the rest/primitive types (int, float, bool, str, ...)
            elif (not isinstance(obj.value, (tuple, list, dict)) and not is_condition_subpath) or is_condition:
                if obj.value != oval:
                    result.append(obj.path, PathDict({ComparisonType.CHANGE.value: (obj.value, oval)}))
                    ct_paths.add(obj.path)

        # Append ignore objects to result
        for path, value in ignore_lst:
            result.append(path, value)
            ct_paths.add(path)

        # Compare other with self (NEW)
        for obj in other.pathitems():

            if isinstance(obj.value, (list, tuple)) or any(obj.path.issuperpath(i) for i in ct_paths):
                continue

            if isinstance(obj.value, self.__class__) and obj.value.get(ComparisonType.IGNORE.value) is not None:
                result.append(obj.path, obj.value)
                ct_paths.add(obj.path)
                continue

            # Ignore path __IGNORE__
            replaced, path = obj.path.replace({v: k for k, v in path_mapping.items()})

            if not replaced and not path.iscontains(ComparisonType.IGNORE.value) and self.get(path) is None:
                result.append(obj.path, PathDict({ComparisonType.NEW.value: obj.value}))
                ct_paths.add(obj.path)

        return func(result, other, **kwargs)
    return wrapper


# PathDict
# ------------------------------------------------------------------------------
class Path(tuple):

    def __new__(cls, *args):
        return tuple.__new__(cls, args)

    def __str__(self):
        return '.'.join(map(str, self))

    @property
    def first(self) -> Tuple:
        sl = self.__len__()
        return self[:(sl if sl <= 1 else -(sl-1))]

    @tostruct
    def isequal(self, other: Tuple) -> bool:
        if len(self) != len(other):
            return False

        idx_lst = [i for i, o in enumerate(other) if o != ASTERISK]
        return [o for i, o in enumerate(self) if i in idx_lst] == [o for i, o in enumerate(other) if i in idx_lst]

    @tostruct
    def iscontains(self, other: Tuple) -> bool:
        i, olen = 0, len(other)
        for o in other:
            if o in self:
                i += 1
                if i == olen:
                    return True
        return False

    @tostruct
    def issubpath(self, other: Tuple) -> bool:
        return other[:len(self)] == self

    @tostruct
    def issuperpath(self, other: Tuple) -> bool:
        return self[:len(other)] == other

    @tostruct
    def isfirst(self, other: Tuple) -> bool:
        return tuple(self[:len(other)]) == other

    @tostruct
    def islast(self, other: Tuple) -> bool:
        return tuple(list(self[::-1][:len(other)])[::-1]) == other

    @tostruct
    def replace(self, mapping: Dict[Tuple, Tuple]) -> Tuple[bool, 'Path']:
        new_path = []

        for old, new in mapping.items():
            if not old:
                return False, self

            if self.issuperpath(old):
                for i, v in enumerate(self):
                    try:
                        new_path.append(new[i])
                    except IndexError:
                        new_path.append(v)
                    except TypeError:
                        break

        if new_path:
            return True, self.__class__(*new_path)

        return False, self


class PathDictItem(tuple):

    def __new__(cls, *args, **kwargs):
        return tuple.__new__(cls)

    def __init__(self, path, value):
        super().__init__()
        self._path = path
        self._value = value

    def __call__(self) -> Tuple[Path, Any]:
        return self.path, self.value

    @property
    def path(self) -> Path:
        return self._path

    @property
    def value(self) -> Any:
        return self._value

    def __repr__(self):
        return '{self.__class__.__name__}({self.path}, {self.value})'.format(self=self)

    def __str__(self):
        return self.__repr__()


class PathDict(dict):

    def __init__(self, seq=None, **kwargs):
        self._od = OrderedDict()
        self._lookup_error = None
        self._conditions = kwargs.pop('conditions', CONDITIONS)
        self._required_paths = kwargs.pop('required_paths', REQUIRED_PATHS)

        if isinstance(seq, list):
            d = dict(seq)
        else:
            d = seq or kwargs

        super().__init__(seq=seq, **kwargs)

        if isinstance(d, dict):
            kwargs = dict(conditions=self._conditions, required_paths=self._required_paths)

            for key, val in d.items():
                if isinstance(val, dict):
                    val = self.__class__(val, **kwargs)

                elif isinstance(val, (list, tuple, set)):
                    val = [self.__class__(v, **kwargs) if isinstance(v, dict) else v for v in val]

                self._od[key] = val

        else:
            raise TypeError(d)

    def to_dict(self) -> dict:
        d = {}  # type: dict

        for key, val in self.items():  # type: str, Union[Dict[Any, Any], Any]
            if isinstance(val, self.__class__):
                val = val.to_dict()  # type: ignore

            elif isinstance(val, (list, tuple)):
                val = [v.to_dict() if isinstance(v, self.__class__) else v for v in val]  # type: ignore

            d[key] = val
        return d

    @pathcondition
    @tostruct
    def search(self, path: Union[str, Tuple[str, int]], default: Any = SENTINEL, rpath: bool = False,
               raise_error: bool = False, condition: Union[Callable, List[Tuple[str, Any]]] = SENTINEL) -> Any:

        self._lookup_error = None
        queue = deque([])  # type: deque

        try:
            obj = self._od
            queue.append(PathDictItem(Path(), obj))

            for i, p in enumerate(path):
                for _ in queue.copy():
                    try:
                        obj = queue.popleft()

                        if p == ASTERISK:
                            if isinstance(obj.value, self.__class__):
                                items = obj.value.items()   # type: ignore
                            else:
                                items = enumerate(obj.value)

                            for ii, o in items:
                                queue.append(PathDictItem(Path(*obj.path+(ii,)), o))

                        elif isinstance(obj.value[p], self.__class__) \
                                and obj.value[p].get(ComparisonType.IGNORE.value) is not None:

                            queue.append(PathDictItem(Path(*obj.path+(p,)), obj.value[p]))

                        else:
                            queue.append(PathDictItem(Path(*obj.path+(p,)), obj.value[p]))

                    except (IndexError, KeyError, TypeError) as e:
                        self._lookup_error = PathNotExistsError(path, type(e))

                        if not raise_error:
                            continue

                        raise self._lookup_error

            if condition is not SENTINEL:
                if not condition:
                    return list(queue)
                return queue

            elif self._lookup_error:
                raise self._lookup_error

            else:
                val_lst = [o if rpath else o.value for o in queue]
                return val_lst[0] if len(val_lst) == 1 else val_lst

        except PathNotExistsError:
            if default is SENTINEL:
                self._lookup_error = PathNotExistsError(path, error_type=SENTINEL)

                if raise_error:
                    self.raise_for_get()
                else:
                    return PathDictItem(path, None) if rpath else None

            return PathDictItem(path, default) if rpath else default

    @tostruct
    def get(self, path: Union[str, Tuple], default: Any = SENTINEL, rpath: bool = False,
            raise_error: bool = False) -> Union['PathDict', PathDictItem]:

        return self.search(path, default, rpath, raise_error)

    def raise_for_get(self) -> Any:
        """ Raises stored :class:`PathNotExistsError`, if one occurred. """

        if self._lookup_error:
            raise type(self._lookup_error)(self._lookup_error)

        return self._lookup_error

    @tostruct
    def set(self, path, value, default=None, dynamic=True, ignore_index=False) -> 'PathDict':
        path_len, pre_path, pre_this, this = len(path), None, None, self._od  # type: int, list, PathDict, PathDict

        for index, path in enumerate(path):

            try:
                this[path]
            except (TypeError, KeyError, IndexError):
                if not dynamic:
                    raise PathNotExistsError(path)

                if isinstance(path, int) or path == ASTERISK:
                    if isinstance(pre_this[pre_path], list):
                        ln = len(pre_this[pre_path])

                    else:
                        ln, pre_this[pre_path] = 0, []

                    if not ignore_index:
                        for i in range(path-ln):
                            pre_this[pre_path].append(default)

                    path, this = pre_path, pre_this[pre_path]

                elif isinstance(this, list):
                    this.append(self.__class__({path: {}}))
                    this, pre_this, pre_path = this[-1], this[-1], path

                elif isinstance(this, dict):
                    this[path] = self.__class__()

                    if index > 0:
                        pre_this, pre_path = this, path

                else:
                    this, pre_this[pre_path] = pre_this[pre_path], self.__class__({})

            except Exception:
                raise Exception()

            if path_len == index+1:
                if isinstance(this, (list, tuple)):
                    try:
                        if ignore_index:
                            raise IndexError()

                        this[path] = value

                    except (TypeError, KeyError, IndexError):
                        this.append(value)

                    path, this = pre_path, pre_this

                elif isinstance(pre_this, (list, tuple)) and isinstance(pre_this[pre_path], dict):
                    pre_this[pre_path][path] = value

                elif ignore_index and isinstance(value, dict) and isinstance(this[path], dict):
                    this[path] = PathDict(list(this[path].items()) + list(value.items()))

                else:
                    this[path] = value

            pre_path, pre_this = path, this

            try:
                this = this[path]
            except TypeError:
                pass

        return self

    @tostruct
    def append(self, path, value) -> 'PathDict':
        return self.set(path, value, ignore_index=True)

    @tostruct
    def delete(self, path) -> Any:
        try:
            del self.get(path[:-1])[path[-1]]
        except (IndexError, KeyError, TypeError, PathNotExistsError):
            raise PathNotExistsError(path)

    @tostruct
    def pop(self, path, default=SENTINEL) -> Any:
        try:
            obj = self.get(path, default)
            self.delete(path)
            return obj
        except (IndexError, KeyError, TypeError, PathNotExistsError):
            if default is SENTINEL:
                raise PathNotExistsError(path)
            return default

    def items(self):
        return self._od.items()

    def keys(self):
        return self._od.keys()

    def values(self):
        return self._od.values()

    def pathitems(self) -> Iterator[Any]:
        def wrapper(item) -> Iterator[Any]:
            yield PathDictItem(Path(), item)

            if not isinstance(item, (dict, list, tuple, set)):
                raise StopIteration()

            elif isinstance(item, dict):
                items = item.items()

            else:
                items = enumerate(item)  # type: ignore

            for key, value in items:
                for o in wrapper(value):
                    yield PathDictItem(Path(*(key,) + o.path), o.value)

        iterator = wrapper(self._od)
        next(iterator)
        return iterator

    @comparator
    def compare(self, other, conditions: Dict[str, Any]=None) -> 'PathDict':
        """ Return the comparison of two PathDict's as a new PathDict. """
        return self

    @comparator
    def intersection(self, other, conditions: Dict[str, Any]=None) -> 'PathDict':
        """
        Return the intersection of two PathDict's as a new PathDict.

        (i.e. all elements that are in both PathDict's.)
        """
        obj = self.__class__()
        for o in self.pathitems():
            if o.path.islast(ComparisonType.EQUAL.value):
                obj.set(o.path[:-1], o.value, ignore_index=True)
        return obj

    @comparator
    def difference(self, other, conditions: Dict[str, Any]=None) -> 'PathDict':
        """
        Return the difference of two PathDict's as a new PathDict.

        (i.e. all elements that are in this PathDict but not the others.)
        """
        obj = self.__class__()
        for o in self.pathitems():
            if o.path.islast(ComparisonType.DELETE.value):
                obj.set(o.path[:-1], o.value, ignore_index=True)
        return obj

    @comparator
    def symmetric_difference(self, other, conditions: Dict[str, Any]=None) -> 'PathDict':
        """
        Return the symmetric difference of two PathDict's as a new PathDict.

        (i.e. all elements that are in exactly one of the PathDict's.)
        """
        obj = self.__class__()
        for o in self.pathitems():
            if any(o.path.islast(t.value) for t in (ComparisonType.DELETE, ComparisonType.NEW)):
                obj.set(o.path[:-1], o.value, ignore_index=True)
        return obj

    def set_comparison_type_by_conditions(self, ct: ComparisonType, conditions: Conditions) -> 'PathDict':

        for path, condition in conditions:
            obj_lst = self.search(path, condition=condition, rpath=True)

            for obj in obj_lst:  # type: PathDictItem
                self.set(obj.path, self.__class__({ct.value: obj.value}))

        return self

    @tobit
    def filter(self, *args) -> 'PathDict':
        """
        Return the filtered PathDict.

        type is an optional string or bit that specifies the ComparisonType(s)
        that is contained in the output. It defaults to 'NCDERI' which
        means 'N' for new, 'C' for change, 'D' for delete, 'E' for equal,
        'R' for required and 'I' for ignore item. The available outputs are:
        ========= ========================================================================
        Character Meaning
        --------- ------------------------------------------------------------------------
        'e'       output all equal items from both objects
        'c'       output all changes items with new value
        'n'       output all new items from the other object
        'd'       output all deleted items from the other object
        'i'       output all ignore items from the other object
        'r'       output all required items from the other object
        'E'       the same as 'e' with output comparison type (__EQUAL__)
        'C'       the same as 'c' with output comparison type (__CHANGE__) and both values
        'N'       the same as 'n' with output comparison type (__NEW__)
        'D'       the same as 'd' with output comparison type (__DELETE__)
        'I'       the same as 'i' with output comparison type (__IGNORE__)
        'R'       the same as 'r' with output comparison type (__REQUIRED__)
        ========= ========================================================================
        """

        obj = self.__class__()  # type: PathDict

        if len(args) == 0:
            return self
        elif len(args) > 1:
            raise ValueError()
        else:
            ct_bit = args[0]

        paths = set()

        for o in self.pathitems():
            for ct in [ct for ct in ComparisonType if ct & ct_bit]:  # type: ComparisonType
                if o.path.islast(ct.value):

                    if isinstance(o.value, list) and any(o.path[:len(p)] == p for p in paths):
                        continue

                    if ct.is_visible():
                        if ct.name == ComparisonType.LEFT_CHANGE.name:
                            value = o.value[0]
                        elif ct.name == ComparisonType.RIGHT_CHANGE.name:
                            value = o.value[1]
                        else:
                            value = o.value
                        obj.append(o.path[:-1], self.__class__({o.path[-1]: value}))

                    else:
                        if ct.name == ComparisonType.left_change.name:
                            value = o.value[0]
                        elif ct == ComparisonType.CHANGE:
                            value = o.value[1]
                        else:
                            value = o.value
                        obj.append(o.path[:-1], value)

                    paths.add(o.path[:-1])
        return obj

    def copy(self) -> 'PathDict':
        return PathDict(self,
                        conditions=deepcopy(self.get_conditions()),
                        required_paths=deepcopy(self.get_required_paths()))

    def get_conditions(self) -> Dict[str, Any]:
        return self._conditions

    def set_conditions(self, conditions: Dict[str, Any]) -> None:
        self._conditions = conditions

    def get_required_paths(self) -> list:
        return self._required_paths

    def set_required_paths(self, required_paths: list) -> None:
        self._required_paths = required_paths

        for p in required_paths:
            self.set_comparison_type_by_conditions(ComparisonType.REQUIRED, [(p, None)])

    @recursive_repr()
    def __repr__(self) -> str:
        if not self._od:
            return '{}()'.format(self.__class__.__name__)
        return '{}({})'.format(self.__class__.__name__, list(self._od.items()))

    def __str__(self) -> str:
        return self.__repr__()

    def __setitem__(self, key, value) -> None:
        self._od[key] = value

    def __getitem__(self, key) -> Any:
        return self._od[key]

    def __setattr__(self, key, value) -> None:
        if key in {'_od', '_lookup_error', '_conditions', '_required_paths'}:
            super().__setattr__(key, value)
        else:
            self[key] = value

    def __getattr__(self, key) -> Any:
        return self[key]

    def __hash__(self) -> int:
        return deephash(self.to_dict())

    def __eq__(self, other) -> bool:
        if isinstance(other, self.__class__):
            other = other.to_dict()  # type: ignore
        return deephash(self.to_dict()) == deephash(other)

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __and__(self, other) -> 'PathDict':
        """ Return self&value. """
        return PathDict.intersection(self, other)

    def __iand__(self, other) -> 'PathDict':
        """ Return self&=value. """
        return self.__and__(other)

    def __sub__(self, other) -> 'PathDict':
        """ Return self-value. """
        return PathDict.difference(self, other)

    def __isub__(self, other) -> 'PathDict':
        """ Return self-=value. """
        return self.__sub__(other)

    def __rsub__(self, other) -> 'PathDict':
        """ Return value-self. """
        return PathDict.__sub__(other, self)

    def __xor__(self, other) -> 'PathDict':
        """ Return self^value. """
        return PathDict.symmetric_difference(self, other)

    def __ixor__(self, other) -> 'PathDict':
        """ Return self^=value. """
        return self.__xor__(other)

    def __contains__(self, key) -> bool:
        return self._od.__contains__(key)

    def __delitem__(self, key) -> None:
        return self._od.__delitem__(key)

    def __delattr__(self, key) -> None:
        return self.__delitem__(key)

    def __copy__(self) -> 'PathDict':
        return self.copy()

    def __deepcopy__(self, memo=None) -> 'PathDict':
        return self.copy()

    def __iter__(self) -> Iterator[Any]:
        return self._od.__iter__()

    def __len__(self) -> int:
        return self._od.__len__()
