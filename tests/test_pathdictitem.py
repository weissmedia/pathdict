from pathdict import (Path, PathDictItem)  # type: ignore


def test_str():
    assert str(PathDictItem(Path(*('a', 0, 'c')), True)) == 'PathDictItem(a.0.c, True)'


def test_repr():
    assert repr(PathDictItem(Path(*('a', 0, 'c')), True)) == 'PathDictItem(a.0.c, True)'
