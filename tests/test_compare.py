import pytest  # type: ignore
from pathdict import (PathDict, ComparisonType as ct, RequiredPathError, DuplicateComparisonTypeError)  # type: ignore
from pathdict import ComparedTypeError # type: ignore
from pprint import pprint


# OBJECT TESTS
# ----------------------------------------------------------------------------------------------------------------------
def test_object_equal():
    a_obj = PathDict({'key': None})
    b_obj = PathDict({'key': None})

    obj_filter = a_obj.compare(b_obj).filter

    assert obj_filter('NCDEI').to_dict() == {'key': {'__EQUAL__': None}}
    assert obj_filter('ncdei').to_dict() == {'key': None}

    a_obj = PathDict({'key': 'value'})
    b_obj = PathDict({'key': 'value'})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'key': {'__EQUAL__': 'value'}}
    assert obj_filter(ct.all).to_dict() == {'key': 'value'}

    a_obj = PathDict({'key': {'subkey': 'value'}})
    b_obj = PathDict({'key': {'subkey': 'value'}})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'key': {'__EQUAL__': {'subkey': 'value'}}}
    assert obj_filter(ct.all).to_dict() == {'key': {'subkey': 'value'}}


def test_object_new1():
    a_obj = PathDict({'key': None})
    b_obj = PathDict({'key1': None})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('N').to_dict() == {'key1': {'__NEW__': None}}
    assert obj_filter('n').to_dict() == {'key1': None}


def test_object_new2():
    a_obj = PathDict({'key': {'subkey': 'value'}})
    b_obj = PathDict({'key': {'subkey1': 'value'}})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.N).to_dict() == {'key': {'subkey1': {'__NEW__': 'value'}}}
    assert obj_filter(ct.n).to_dict() == {'key': {'subkey1': 'value'}}


def test_object_delete():
    a_obj = PathDict({'key': None})
    b_obj = PathDict({'key1': None})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('D').to_dict() == {'key': {'__DELETE__': None}}
    assert obj_filter('d').to_dict() == {'key': None}

    a_obj = PathDict({'key': {'subkey': 'value'}})
    b_obj = PathDict({'key': {'subkey1': 'value'}})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.D).to_dict() == {'key': {'subkey': {'__DELETE__': 'value'}}}
    assert obj_filter(ct.d).to_dict() == {'key': {'subkey': 'value'}}


def test_object_change():
    a_obj = PathDict({'key': None})
    b_obj = PathDict({'key': 'value'})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('C').to_dict() == {'key': {'__CHANGE__': [None, 'value']}}
    assert obj_filter('c').to_dict() == {'key': 'value'}

    a_obj = PathDict({'key': {'subkey': 'value'}})
    b_obj = PathDict({'key': {'subkey': 'value1'}})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.C).to_dict() == {'key': {'subkey': {'__CHANGE__': ['value', 'value1']}}}
    assert obj_filter(ct.c).to_dict() == {'key': {'subkey': 'value1'}}


def test_object_new_change_delete():
    a_obj = PathDict({'key': {'subkey': 'value'}, 'key1': {'subkey': 'value'}})
    b_obj = PathDict({'key': {'subkey': 'value1'}, 'key1': {'subkey1': 'value'}})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('NCDEI').to_dict() == {'key': {'subkey': {'__CHANGE__': ['value', 'value1']}},
                                             'key1': {'subkey': {'__DELETE__': 'value'},
                                                      'subkey1': {'__NEW__': 'value'}}}

    assert obj_filter('ncdei').to_dict() == {'key': {'subkey': 'value1'},
                                             'key1': {'subkey': 'value', 'subkey1': 'value'}}

    assert obj_filter('N').to_dict() == {'key1': {'subkey1': {'__NEW__': 'value'}}}
    assert obj_filter('n').to_dict() == {'key1': {'subkey1': 'value'}}

    assert obj_filter('C').to_dict() == {'key': {'subkey': {'__CHANGE__': ['value', 'value1']}}}
    assert obj_filter('c').to_dict() == {'key': {'subkey': 'value1'}}

    assert obj_filter('D').to_dict() == {'key1': {'subkey': {'__DELETE__': 'value'}}}
    assert obj_filter('d').to_dict() == {'key1': {'subkey': 'value'}}


# LIST TESTS
# ----------------------------------------------------------------------------------------------------------------------
def test_list_equal():
    a_obj = PathDict({'key': []})
    b_obj = PathDict({'key': []})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('NCDEI').to_dict() == {'key': {'__EQUAL__': []}}
    assert obj_filter('ncdei').to_dict() == {'key': []}

    a_obj = PathDict({'key': [{'key': 'value'}]})
    b_obj = PathDict({'key': [{'key': 'value'}]})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.N | ct.C | ct.D | ct.E | ct.I).to_dict() == {'key': {'__EQUAL__': [{'key': 'value'}]}}
    assert obj_filter(ct.n | ct.c | ct.d | ct.e | ct.i).to_dict() == {'key': [{'key': 'value'}]}

    a_obj = PathDict({'key': [{'key': 'value'}, {'key2': 'value2'}]})
    b_obj = PathDict({'key': [{'key': 'value'}, {'key2': 'value2'}]})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.NEW | ct.CHANGE | ct.DELETE | ct.EQUAL | ct.IGNORE).to_dict() == \
           {'key': {'__EQUAL__': [{'key': 'value'}, {'key2': 'value2'}]}}
    assert obj_filter(ct.new | ct.change | ct.delete | ct.equal | ct.ignore).to_dict() == \
           {'key': [{'key': 'value'}, {'key2': 'value2'}]}


def test_list_new():
    a_obj = PathDict({'key': []})
    b_obj = PathDict({'key': [{'key': 'value'}]})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('N').to_dict() == {'key': [{'__NEW__': {'key': 'value'}}]}
    assert obj_filter('n').to_dict() == {'key': [{'key': 'value'}]}

    a_obj = PathDict({'key': [{'key': 'value'}]})
    b_obj = PathDict({'key': [{'key': 'value'}, {'key1': 'value1'}]})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('N').to_dict() == {'key': [{'__NEW__': {'key1': 'value1'}}]}
    assert obj_filter('n').to_dict() == {'key': [{'key1': 'value1'}]}


def test_list_delete():
    a_obj = PathDict({'key': [{'key': 'value'}]})
    b_obj = PathDict({'key': []})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('D').to_dict() == {'key': [{'__DELETE__': {'key': 'value'}}]}
    assert obj_filter('d').to_dict() == {'key': [{'key': 'value'}]}

    a_obj = PathDict({'key': [{'key': 'value'}]})
    b_obj = PathDict({'key': [{'key1': 'value'}]})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('D').to_dict() == {'key': [{'key': {'__DELETE__': 'value'}}]}
    assert obj_filter('d').to_dict() == {'key': [{'key': 'value'}]}


def test_list_change():
    a_obj = PathDict({'key': [{'key': 'value'}]})
    b_obj = PathDict({'key': [{'key': 'value1'}]})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('C').to_dict() == {'key': [{'key': {'__CHANGE__': ['value', 'value1']}}]}
    assert obj_filter('c').to_dict() == {'key': [{'key': 'value1'}]}

    a_obj = PathDict({'key': [{'key': [{'key': 'value'}]}]})
    b_obj = PathDict({'key': [{'key': [{'key': 'value1'}]}]})
    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter('C').to_dict() == {'key': [{'key': [{'key': {'__CHANGE__': ['value', 'value1']}}]}]}
    assert obj_filter('c').to_dict() == {'key': [{'key': [{'key': 'value1'}]}]}


# COLLATOR LIST TESTS
# ----------------------------------------------------------------------------------------------------------------------
def test_set_conditions():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'}, {'key': 'value1', 'type': '2'}]}})
    b_obj = PathDict({'root': {'parent': [{'key': 'value1', 'type': '_'}, {'key': 'value', 'type': '1'}]}})

    assert a_obj.compare(b_obj).to_dict() == {'root': {'parent': [{'key': {'__CHANGE__': ['value', 'value1']},
                                                                   'type': {'__CHANGE__': ['1', '_']}},
                                                                  {'key': {'__CHANGE__': ['value1', 'value']},
                                                                   'type': {'__CHANGE__': ['2', '1']}}]}}

    a_obj.set_conditions({'root.parent': ['type']})

    assert a_obj.compare(b_obj).to_dict() == {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                                                  {'__DELETE__': {'key': 'value1', 'type': '2'}},
                                                                  {'__NEW__': {'key': 'value1', 'type': '_'}}]}}


def test_conditions_list_equal():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'}, {'key': 'value1', 'type': '2'}]}})
    b_obj = PathDict({'root': {'parent': [{'key': 'value1', 'type': '2'}, {'key': 'value', 'type': '1'}]}})

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['type']}).filter

    assert obj_filter('E').to_dict() == {'root': {'__EQUAL__': {'parent': [{'type': '1', 'key': 'value'},
                                                                           {'type': '2', 'key': 'value1'}]}}}

    assert obj_filter('e').to_dict() == {'root': {'parent': [{'key': 'value', 'type': '1'},
                                                             {'key': 'value1', 'type': '2'}]}}


def test_conditions_list_new():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value1', 'type': '2'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '3'}]}})

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['type']}).filter

    assert obj_filter('N').to_dict() == {'root': {'parent': [{'__NEW__': {'key': 'value', 'type': '3'}}]}}
    assert obj_filter('n').to_dict() == {'root': {'parent': [{'key': 'value', 'type': '3'}]}}


def test_conditions_list_delete():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value1', 'type': '2'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value1', 'type': '3'},
                                         {'key': 'value', 'type': '1'}]}})

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['type']}).filter

    assert obj_filter('D').to_dict() == {'root': {'parent': [{'__DELETE__': {'key': 'value1', 'type': '2'}}]}}
    assert obj_filter('d').to_dict() == {'root': {'parent': [{'key': 'value1', 'type': '2'}]}}


# COLLATOR IGNORE CONDITIONS LIST TESTS
# ----------------------------------------------------------------------------------------------------------------------
def test_conditions_list_ignore_change_new_equal_delete():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '2'},
                                         {'key': 'value', 'type': '3'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '4'},
                                         {'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '4'},
                                         {'key': 'value', 'type': '1'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent', lambda i, val: i == len(val)-2)])

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['type']}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                                                 {'__CHANGE__': [{'key': 'value', 'type': '2'},
                                                                                 {'key': 'value1', 'type': '2'}]},
                                                                 {'__DELETE__': {'key': 'value', 'type': '3'}},
                                                                 {'__IGNORE__': {'key': 'value', 'type': '4'}},
                                                                 {'__NEW__': {'key': 'value', 'type': '4'}}]}}

    assert obj_filter('ncdei').to_dict() == {'root': {'parent': [{'key': 'value', 'type': '1'},
                                                                          {'key': 'value1', 'type': '2'},
                                                                          {'key': 'value', 'type': '3'},
                                                                          {'key': 'value', 'type': '4'},
                                                                          {'key': 'value', 'type': '4'}]}}

    assert obj_filter('I').to_dict() == {'root': {'parent': [{'__IGNORE__': {'type': '4', 'key': 'value'}}]}}
    assert obj_filter('i').to_dict() == {'root': {'parent': [{'type': '4', 'key': 'value'}]}}

    assert obj_filter('D').to_dict() == {'root': {'parent': [{'__DELETE__': {'type': '3', 'key': 'value'}}]}}
    assert obj_filter('d').to_dict() == {'root': {'parent': [{'type': '3', 'key': 'value'}]}}

    assert obj_filter('E').to_dict() == {'root': {'parent': [{'__EQUAL__': {'type': '1', 'key': 'value'}}]}}
    assert obj_filter('e').to_dict() == {'root': {'parent': [{'type': '1', 'key': 'value'}]}}

    assert obj_filter('N').to_dict() == {'root': {'parent': [{'__NEW__': {'type': '4', 'key': 'value'}}]}}
    assert obj_filter('n').to_dict() == {'root': {'parent': [{'type': '4', 'key': 'value'}]}}

    assert obj_filter('C').to_dict() == {'root': {'parent': [{'__CHANGE__': [{'type': '2', 'key': 'value'},
                                                                             {'type': '2', 'key': 'value1'}]}]}}
    assert obj_filter('c').to_dict() == {'root': {'parent': [{'type': '2', 'key': 'value1'}]}}


def test_conditions_list_ignore_change():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '2'},
                                         {'key': 'value', 'type': '3'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '4'},
                                         {'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '4'},
                                         {'key': 'value', 'type': '1'}]}})

    ignores = [
        ('root.parent', lambda i, val: i == len(val) - 2),
    ]
    b_obj.set_comparison_type_by_conditions(ct.IGNORE, ignores)

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['type']}).filter

    assert obj_filter('C').to_dict() == {'root': {'parent': [{'__CHANGE__': [{'type': '2', 'key': 'value'},
                                                                             {'type': '2', 'key': 'value1'}]}]}}
    assert obj_filter('c').to_dict() == {'root': {'parent': [{'type': '2', 'key': 'value1'}]}}


def test_conditions_list_ignore_new():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '2'},
                                         {'key': 'value', 'type': '3'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '4'},
                                         {'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '4'},
                                         {'key': 'value', 'type': '1'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent', lambda i, val: i == len(val) - 2)])

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['type']}).filter

    assert obj_filter('N').to_dict() == {'root': {'parent': [{'__NEW__': {'type': '4', 'key': 'value'}}]}}
    assert obj_filter('n').to_dict() == {'root': {'parent': [{'type': '4', 'key': 'value'}]}}


def test_conditions_list_ignore_equal():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '2'},
                                         {'key': 'value', 'type': '3'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '4'},
                                         {'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '4'},
                                         {'key': 'value', 'type': '1'}]}})

    ignores = [
        ('root.parent', lambda i, val: i == len(val) - 2),
    ]
    b_obj.set_comparison_type_by_conditions(ct.IGNORE, ignores)
    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['type']}).filter

    assert obj_filter('E').to_dict() == {'root': {'parent': [{'__EQUAL__': {'type': '1', 'key': 'value'}}]}}
    assert obj_filter('e').to_dict() == {'root': {'parent': [{'type': '1', 'key': 'value'}]}}


def test_conditions_list_ignore_delete():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '2'},
                                         {'key': 'value', 'type': '3'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '4'},
                                         {'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '4'},
                                         {'key': 'value', 'type': '1'}]}})

    ignores = [
        ('root.parent', lambda i, val: i == len(val) - 2),
    ]
    b_obj.set_comparison_type_by_conditions(ct.IGNORE, ignores)
    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['type']}).filter

    assert obj_filter('D').to_dict() == {'root': {'parent': [{'__DELETE__': {'type': '3', 'key': 'value'}}]}}
    assert obj_filter('d').to_dict() == {'root': {'parent': [{'type': '3', 'key': 'value'}]}}


def test_conditions_list_ignore_change_new_equal_delete_multi_conditions():
    a_obj = PathDict({'root': {'parent': [{'a': 'a1', 'b': 'b1'},
                                         {'a': 'a2', 'b': 'b2'},
                                         {'a': 'a3', 'b': 'b3'}]}})

    b_obj = PathDict({'root': {'parent': [{'a': 'a1', 'b': 'b0'},
                                         {'a': 'a2', 'b': 'b2'},
                                         {'a': 'a1', 'b': 'b0'},
                                         {'a': 'a3', 'b': 'b3'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent', lambda i, val: i == len(val) - 2)])

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['a', 'b']}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'parent': [{'__DELETE__': {'a': 'a1', 'b': 'b1'}},
                                                                 {'__EQUAL__': {'a': 'a2', 'b': 'b2'}},
                                                                 {'__EQUAL__': {'a': 'a3', 'b': 'b3'}},
                                                                 {'__IGNORE__': {'a': 'a1', 'b': 'b0'}},
                                                                 {'__NEW__': {'a': 'a1', 'b': 'b0'}}]}}


def test_list_equal_order_random():
    a_obj = PathDict({'root': [{'sa': 'mr', 'fn': 'ma', 'ln': 'gow', 'bd': '19790424', 'ag': 38},
                              {'sa': 'mrs', 'fn': 'mi', 'ln': 'gow', 'bd': '19781020', 'ag': 39},
                              {'sa': 'mrs', 'fn': 'nw', 'ln': 'gow', 'bd': '20021130', 'ag': 15},
                              {'sa': 'mrs', 'fn': 'va', 'ln': 'gow', 'bd': '20110529', 'ag': 6}]})

    b_obj = PathDict({'root': [{'ag': 38, 'bd': '19790424', 'ln': 'gow', 'fn': 'ma', 'sa': 'mr'},
                              {'ag': 39, 'bd': '19781020', 'ln': 'gow', 'fn': 'mi', 'sa': 'mrs'},
                              {'ag': 15, 'bd': '20021130', 'ln': 'gow', 'fn': 'nw', 'sa': 'mrs'},
                              {'ag': 6, 'bd': '20110529', 'ln': 'gow', 'fn': 'va', 'sa': 'mrs'}]})

    obj_filter = a_obj.compare(b_obj, conditions={'root': ['ag', 'bd', 'ln', 'fn', 'sa']}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': [
        {'__EQUAL__': {'sa': 'mr', 'fn': 'ma', 'ln': 'gow', 'ag': 38, 'bd': '19790424'}},
        {'__EQUAL__': {'sa': 'mrs', 'fn': 'mi', 'ln': 'gow', 'ag': 39, 'bd': '19781020'}},
        {'__EQUAL__': {'sa': 'mrs', 'fn': 'nw', 'ln': 'gow', 'ag': 15, 'bd': '20021130'}},
        {'__EQUAL__': {'sa': 'mrs', 'fn': 'va', 'ln': 'gow', 'ag': 6, 'bd': '20110529'}}]}


def test_dict_equal_order_random():
    a_obj = PathDict({'co': {'sa': 'mr', 'fn': 'ma', 'ln': 'gow', 'ad': {
        'st': 'fri', 'hn': '22', 'pc': '65795', 'ci': 'hatt', 'co': 'de'},
                            'em': 'gow', 'te': '015785539181'}})

    b_obj = PathDict({'co': {'em': 'gow', 'ad': {
        'ci': 'hatt', 'st': 'fri', 'co': 'de', 'pc': '65795', 'hn': '22'},
                            'ln': 'gow', 'fn': 'ma', 'te': '015785539181', 'sa': 'mr'}})

    obj_filter = a_obj.compare(b_obj, conditions={'co': None}).filter

    assert obj_filter('NCDEI').to_dict() == {
        'co': {'__EQUAL__': {'sa': 'mr', 'ad': {'st': 'fri', 'hn': '22', 'pc': '65795', 'co': 'de', 'ci': 'hatt'},
                             'ln': 'gow', 'fn': 'ma', 'te': '015785539181', 'em': 'gow'}}}


def test_nested_list_with_condition_equal():
    a_obj = PathDict({'root': {'child': [{'childchild': [{'t': 2, 'num': 145, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 1, 'num': 145, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    b_obj = PathDict({'root': {'child': [{'childchild': [{'t': 1, 'num': 145, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 2, 'num': 145, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    obj_filter = a_obj.compare(b_obj, conditions={'root.child.*.childchild': ['t']}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'__EQUAL__': {'child': [
        {'childchild': [{'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 2},
                        {'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 1}]}]}}}

    obj_filter = a_obj.compare(b_obj, conditions={'root.child.*.childchild.*': ['t']}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'__EQUAL__': {'child': [
        {'childchild': [{'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 2},
                        {'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 1}]}]}}}

    obj_filter = a_obj.compare(b_obj, conditions={'root.child.*.childchild.*': None}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'__EQUAL__': {'child': [
        {'childchild': [{'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 2},
                        {'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 1}]}]}}}


def test_nested_list_with_condition_not_equal_none_asterisk():
    a_obj = PathDict({'root': {'child': [{'childchild': [{'t': 2, 'num': 145, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 1, 'num': 145, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    b_obj = PathDict({'root': {'child': [{'childchild': [{'t': 1, 'num': 1450, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 2, 'num': 1450, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    obj_filter = a_obj.compare(b_obj, conditions={'root.child.*.childchild.*': None}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'child': [{'childchild': [
        {'__DELETE__': {'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 2}},
        {'__DELETE__': {'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 1}},
        {'__NEW__': {'date': '2018-04-02T16:00:00+0200', 'num': 1450, 't': 1}},
        {'__NEW__': {'date': '2018-04-02T16:00:00+0200', 'num': 1450, 't': 2}}]}]}}


def test_nested_list_with_condition_not_equal_none():
    a_obj = PathDict({'root': {'child': [{'childchild': [{'t': 2, 'num': 145, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 1, 'num': 145, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    b_obj = PathDict({'root': {'child': [{'childchild': [{'t': 1, 'num': 1450, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 2, 'num': 1450, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    obj_filter = a_obj.compare(b_obj, conditions={'root.child.*.childchild': None}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'child': [{'childchild': {'__CHANGE__': [
        [{'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 2},
         {'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 1}],
        [{'date': '2018-04-02T16:00:00+0200', 'num': 1450, 't': 1},
         {'date': '2018-04-02T16:00:00+0200', 'num': 1450, 't': 2}]]}}]}}


def test_nested_list_with_condition_not_equal():
    a_obj = PathDict({'root': {'child': [{'childchild': [{'t': 2, 'num': 145, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 1, 'num': 145, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    b_obj = PathDict({'root': {'child': [{'childchild': [{'t': 1, 'num': 1450, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 2, 'num': 1450, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    obj_filter = a_obj.compare(b_obj, conditions={'root.child.*.childchild': ['t']}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'child': [{'childchild': [
        {'__CHANGE__': [{'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 2},
                        {'date': '2018-04-02T16:00:00+0200', 'num': 1450, 't': 2}]},
        {'__CHANGE__': [{'date': '2018-04-02T16:00:00+0200', 'num': 145, 't': 1},
                        {'date': '2018-04-02T16:00:00+0200', 'num': 1450, 't': 1}]}]}]}}


def test_nested_list_with_condition_not_equal_asterisk():
    a_obj = PathDict({'root': {'child': [{'childchild': [{'t': 2, 'num': 145, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 1, 'num': 145, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    b_obj = PathDict({'root': {'child': [{'childchild': [{'t': 1, 'num': 1450, 'date': '2018-04-02T16:00:00+0200'},
                                                         {'t': 2, 'num': 1450, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    with pytest.warns(UserWarning):
        a_obj.compare(b_obj, conditions={'root.child.*.childchild.*': ['t']})


def test_nested_list_with_none_condition():
    a_obj = PathDict({'root': {'child': [{'childchild': [{'num': 145, 'date': '2018-04-02T16:00:00+0200'}]}]}})
    b_obj = PathDict({'root': {'child': [{'childchild': [{'num': 1450, 'date': '2018-04-02T16:00:00+0200'}]}]}})

    obj_filter = a_obj.compare(b_obj, conditions={'root.child.*.childchild.*': None}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'child': [{'childchild': [
        {'__DELETE__': {'date': '2018-04-02T16:00:00+0200', 'num': 145}},
        {'__NEW__': {'date': '2018-04-02T16:00:00+0200', 'num': 1450}}]}]}}


def test_intersection():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '2'},
                                         {'key': 'value', 'type': '3'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '4'},
                                         {'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '4'},
                                         {'key': 'value', 'type': '1'}]}})

    obj = a_obj.intersection(b_obj, conditions={'root.parent': ['type']})

    assert obj.to_dict() == {'root': {'parent': [{'key': 'value', 'type': '1'}]}}


def test_difference():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '2'},
                                         {'key': 'value', 'type': '3'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '4'},
                                         {'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '4'},
                                         {'key': 'value', 'type': '1'}]}})

    obj = a_obj.difference(b_obj, conditions={'root.parent': ['type']})

    assert obj.to_dict() == {'root': {'parent': [{'key': 'value', 'type': '3'}]}}


def test_symmetric_difference():
    a_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '1'},
                                         {'key': 'value', 'type': '2'},
                                         {'key': 'value', 'type': '3'}]}})

    b_obj = PathDict({'root': {'parent': [{'key': 'value', 'type': '4'},
                                         {'key': 'value1', 'type': '2'},
                                         {'key': 'value', 'type': '4'},
                                         {'key': 'value', 'type': '1'}]}})

    obj = a_obj.symmetric_difference(b_obj, conditions={'root.parent': ['type']})

    assert obj.to_dict() == {'root': {'parent': [{'key': 'value', 'type': '3'},
                                                 {'key': 'value', 'type': '4'},
                                                 {'key': 'value', 'type': '4'}]}}
    
    
def test_one_item_b_ignore_then_a_delete():
    a_obj = PathDict({'root': {'parent': [{'a': 'a1', 'b': 'b1'}]}})
    b_obj = PathDict({'root': {'parent': [{'a': 'a1', 'b': 'b0'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent', [('b', ['b0'])])])

    a_obj.set_conditions({'root.parent': [('a', ['a1'])]})
    obj_filter = a_obj.compare(b_obj).filter

    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent': [{'__DELETE__': {'a': 'a1', 'b': 'b1'}},
                                                                {'__IGNORE__': {'a': 'a1', 'b': 'b0'}}]}}


def test_different_item_b_ignore_then_a_delete():

    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})

    b_obj = PathDict({'root': {'parent': [{'a': 'a1', 'b': 'b0'},
                                          {'a': 'a2', 'b': 'b2'},
                                          {'a': 'a2', 'b': 'b0'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent', [('b', ['b1', 'b0'])])])

    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent': [{'__EQUAL__': {'a': 'a2', 'b': 'b2'}},
                                                                {'__IGNORE__': {'a': 'a1', 'b': 'b0'}},
                                                                {'__IGNORE__': {'a': 'a2', 'b': 'b0'}}]}}


def test_compare_primitive_types():
    a_obj = PathDict({'root': {'parent': {'a': 'a1', 'b': 'b1'}}})

    b_obj = PathDict({'root': {'parent': {'b': 'b1'}}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent', None)])

    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent': {'__DELETE__': {'a': 'a1', 'b': 'b1'},
                                                                '__IGNORE__': {'b': 'b1'}}}}


def test_required_paths():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})

    a_obj.set_required_paths(['root.parent.*.a'])

    obj_filter = a_obj.compare(b_obj).filter

    assert obj_filter(ct.REQUIRED).to_dict() == {'root': {'parent': [{'a': {'__REQUIRED__': 'a2'}}]}}
    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent': [{'a': {'__REQUIRED__': 'a2'},
                                                                 'b': {'__EQUAL__': 'b2'}}]}}


def test_required_paths_continue():

    a_obj = PathDict({'root': {'id': 1, 'parent': [{'a': 'a2', 'b': 'b2'}]}})

    b_obj = PathDict({'root': {'id': 1, 'parent': [{'a': 'a1', 'b': 'b0'},
                                                   {'a': 'a2', 'b': 'b2'},
                                                   {'a': 'a2', 'b': 'b0'}]}})

    a_obj.set_required_paths(['root.id'])

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent', [('b', ['b1', 'b0'])])])

    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'root': {'id': {'__REQUIRED__': 1},
                                                     'parent': [{'__EQUAL__': {'a': 'a2', 'b': 'b2'}},
                                                                {'__IGNORE__': {'a': 'a1', 'b': 'b0'}},
                                                                {'__IGNORE__': {'a': 'a2', 'b': 'b0'}}]}}


def test_required_path_value_error():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a': '_', 'b': 'b2'}]}})

    a_obj.set_required_paths(['root.parent.*.a'])

    with pytest.raises(ValueError):
        a_obj.compare(b_obj)


def test_required_paths_error():
    a_obj = PathDict({'root': {'parent': [{'_': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})

    a_obj.set_required_paths(['root.parent.*.a'])

    with pytest.raises(RequiredPathError):
        a_obj.compare(b_obj)


def test_hash():
    a_obj = PathDict({'root': [{'sa': 'mr', 'fn': 'ma', 'ln': 'gow', 'bd': '19790424', 'ag': 38},
                              {'sa': 'mrs', 'fn': 'mi', 'ln': 'gow', 'bd': '19781020', 'ag': 39},
                              {'sa': 'mrs', 'fn': 'nw', 'ln': 'gow', 'bd': '20021130', 'ag': 15},
                              {'sa': 'mrs', 'fn': 'va', 'ln': 'gow', 'bd': '20110529', 'ag': 6}]})

    b_obj = PathDict({'root': [{'ag': 38, 'bd': '19790424', 'ln': 'gow', 'fn': 'ma', 'sa': 'mr'},
                               {'ag': 39, 'bd': '19781020', 'ln': 'gow', 'fn': 'mi', 'sa': 'mrs'},
                               {'ag': 15, 'bd': '20021130', 'ln': 'gow', 'fn': 'nw', 'sa': 'mrs'},
                               {'ag': 6, 'bd': '20110529', 'ln': 'gow', 'fn': 'va', 'sa': 'mrs'}]})

    assert hash(a_obj) == hash(b_obj)


def test_and():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a': 'a3', 'b': 'b2'}]}})

    assert a_obj & b_obj == PathDict({'root': {'parent': [{'b': 'b2'}]}})


def test_iand():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a': 'a3', 'b': 'b2'}]}})
    a_obj &= b_obj

    assert a_obj == PathDict({'root': {'parent': [{'b': 'b2'}]}})


def test_sub():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a1': 'a2', 'b': 'b2'}]}})

    assert a_obj-b_obj == PathDict({'root': {'parent': [{'a': 'a2'}]}})


def test_isub():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a1': 'a2', 'b': 'b2'}]}})
    a_obj -= b_obj

    assert a_obj == PathDict({'root': {'parent': [{'a': 'a2'}]}})


def test_rsub():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a1': 'a2', 'b': 'b2'}]}})

    assert a_obj.__rsub__(b_obj) == PathDict({'root': {'parent': [{'a1': 'a2'}]}})


def test_xor():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a1': 'a2', 'b': 'b2'}]}})

    assert a_obj ^ b_obj == {'root': {'parent': [{'a': 'a2', 'a1': 'a2'}]}}


def test_ixor():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a1': 'a2', 'b': 'b2'}]}})
    a_obj ^= b_obj

    assert a_obj == {'root': {'parent': [{'a': 'a2', 'a1': 'a2'}]}}


def test_iter():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})

    assert next(iter(a_obj)) == 'root'


def test_str():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2'}]}})

    assert str(a_obj) == "PathDict([('root', PathDict([('parent', [PathDict([('a', 'a2')])])]))])"


def test_repr():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2'}]}})

    assert repr(a_obj) == "PathDict([('root', PathDict([('parent', [PathDict([('a', 'a2')])])]))])"
    assert repr(PathDict()) == 'PathDict()'


def test_duplicate_comparison_type_error():
    a_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})
    b_obj = PathDict({'root': {'parent': [{'a': 'a2', 'b': 'b2'}]}})

    with pytest.raises(DuplicateComparisonTypeError):
        a_obj.compare(b_obj).filter('NCDIRn')


def test_other_precondition ():
    a_obj = PathDict({'root': {'parent': [{'a': 'a1', 'b': 'b1'},
                                          {'a': 'a2', 'b': 'b2'},
                                          {'a': 'a3', 'b': 'b3'}]}})

    b_obj = PathDict({'root': {'parent': [{'a': 'a1', 'b': 'b0'},
                                          {'a': 'a2', 'b': 'b2'},
                                          {'a': 'a1', 'b': 'b0'},
                                          {'a': 'a3', 'b': 'b3'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent', lambda i, val: i == len(val) - 2)])
    b_obj.set_comparison_type_by_conditions(ct.NEW, [('root.parent', [('a', ['a1']), ('b', ['b0'])])])
    b_obj.set_comparison_type_by_conditions(ct.EQUAL, [('root.parent', [('a', ['a2', 'a3']), ('b', ['b2', 'b3'])])])
    a_obj.set_comparison_type_by_conditions(ct.DELETE, [('root.parent', [('a', ['a1']), ('b', ['b1'])])])

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': ['a', 'b']}).filter

    assert obj_filter('NCDEI').to_dict() == {'root': {'parent': [{'__DELETE__': {'a': 'a1', 'b': 'b1'}},
                                                                 {'__IGNORE__': {'a': 'a1', 'b': 'b0'}},
                                                                 {'__EQUAL__': {'a': 'a2', 'b': 'b2'}},
                                                                 {'__NEW__': {'a': 'a1', 'b': 'b0'}},
                                                                 {'__EQUAL__': {'a': 'a3', 'b': 'b3'}}]}}


def test_compare_different_type_error():
    a_obj = PathDict({'root': {'parent': [{'a': 'a1', 'b': 'b1'},
                                          {'a': 'a2', 'b': 'b2'},
                                          {'a': 'a3', 'b': 'b3'}]}})

    b_obj = PathDict({'root': {'parent': {'a': 'a1', 'b': 'b0'}}})

    with pytest.raises(ComparedTypeError):
        a_obj.compare(b_obj)


def test_duplicate_ignore_and_delete():
    a_obj = PathDict({'root': {'parent1': [{'t': 3, 'b': 'b1'},
                                           {'t': 3, 'b': 'b2'}],
                               'parent2': [{'t': 2, 'b': 'b1'},
                                           {'t': 1, 'b': 'b2'}]
                                }})

    b_obj = PathDict({'root': {'parent1': [{'t': 1, 'b': 'b1'},
                                           {'t': 2, 'b': 'b2'}],
                               'parent2': [{'t': 2, 'b': 'b3'},
                                           {'t': 1, 'b': 'b4'}]
                               }})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.*.*', [('t', [1, 2])])])

    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent1': [{'__DELETE__': {'b': 'b1', 't': 3}},
                                                                 {'__DELETE__': {'b': 'b2', 't': 3}},
                                                                 {'__IGNORE__': {'b': 'b1', 't': 1}},
                                                                 {'__IGNORE__': {'b': 'b2', 't': 2}}],
                                                     'parent2': [{'__DELETE__': {'b': 'b1', 't': 2}},
                                                                 {'__DELETE__': {'b': 'b2', 't': 1}},
                                                                 {'__IGNORE__': {'b': 'b3', 't': 2}},
                                                                 {'__IGNORE__': {'b': 'b4', 't': 1}}]}}


def test_compare_different_list_count():
    a_obj = PathDict({'root': {'parent2': []}})

    b_obj = PathDict({'root': {'parent': [{'a': '1', 'b': 'b0'},
                                          {'a': '2', 'b': 'b1'},
                                          {'a': '2', 'b': 'b2'}],
                               'parent2': []}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent.*', [('a', ['2'])])])

    obj_filter = a_obj.compare(b_obj, conditions={'root.parent': None}).filter

    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent': [{'__NEW__': {'a': '1', 'b': 'b0'}},
                                                                {'__IGNORE__': {'a': '2', 'b': 'b1'}},
                                                                {'__IGNORE__': {'a': '2', 'b': 'b2'}}],
                                                     'parent2': {'__EQUAL__': []}}}


def test_self_two_other_three_and_one_ignore_rest_equal():
    a_obj = PathDict({'root': {'parent': [{'a': '1', 'b': 'b0'},
                                          {'a': '3', 'b': 'b3'}]}})

    b_obj = PathDict({'root': {'parent': [{'a': '1', 'b': 'b0'},
                                          {'a': '2', 'b': 'b1'},
                                          {'a': '3', 'b': 'b3'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent.*', [('a', ['2'])])])

    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent': [{'__EQUAL__': {'a': '1', 'b': 'b0'}},
                                                                {'__EQUAL__': {'a': '3', 'b': 'b3'}},
                                                                {'__IGNORE__': {'a': '2', 'b': 'b1'}}]}}


def test_self_two_other_three_and_one_ignore_rest_equal_change():
    a_obj = PathDict({'root': {'parent': [{'a': '1', 'b': 'b0'},
                                          {'a': '3', 'b': 'b3'}]}})

    b_obj = PathDict({'root': {'parent': [{'a': '1', 'b': 'b0'},
                                          {'a': '2', 'b': 'b1'},
                                          {'a': '4', 'b': 'b4'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent.*', [('a', ['2'])])])

    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent': [{'__EQUAL__': {'a': '1', 'b': 'b0'}},
                                                                {'a': {'__CHANGE__': ['3', '4']},
                                                                 'b': {'__CHANGE__': ['b3', 'b4']}},
                                                                {'__IGNORE__': {'a': '2', 'b': 'b1'}}]}}


def test_self_two_other_three_and_two_ignore_rest_change_delete():
    a_obj = PathDict({'root': {'parent': [{'a': '1', 'b': 'b0'},
                                          {'a': '3', 'b': 'b3'}]}})

    b_obj = PathDict({'root': {'parent': [{'a': '2', 'b': 'b0'},
                                          {'a': '2', 'b': 'b1'},
                                          {'a': '3', 'b': 'b4'}]}})

    b_obj.set_comparison_type_by_conditions(ct.IGNORE, [('root.parent.*', [('a', ['2'])])])

    obj_filter = a_obj.compare(b_obj).filter
    assert obj_filter(ct.ALL).to_dict() == {'root': {'parent': [{'a': {'__CHANGE__': ['1', '3']},
                                                                 'b': {'__CHANGE__': ['b0', 'b4']}},
                                                                {'__DELETE__': {'a': '3', 'b': 'b3'}},
                                                                {'__IGNORE__': {'a': '2', 'b': 'b0'}},
                                                                {'__IGNORE__': {'a': '2', 'b': 'b1'}}]}}
