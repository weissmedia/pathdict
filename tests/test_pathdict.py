import pytest  # type: ignore
from collections import OrderedDict
from pathdict import (PathDict, PathDictItem, deephash)  # type: ignore
from pathdict.utils import ComparisonType as ct  # type: ignore
from pathdict.exceptions import PathNotExistsError  # type: ignore
from pprint import pprint


@pytest.fixture()
def complex_object():
    return PathDict({'root': {'parent2': [
        {'child0': []},
        {'child1': [1, 2, 3]},
        {'child2': [
            {
                'subchild1': 'value',
                'subchild2': 'value1',
                'type': 'value1',
                'id': 1,
                'subchild3': {
                    'subsubchild1': 'value4',
                    'subsubchild2': 'value4',
                    'subsubchild3': [{
                        'subsubchild4': 'value10',
                        'subsubchild5': 'value11'
                    }]
                }

            },
            {
                'subchild1': 'value',
                'subchild2': 'value2',
                'type': 'value3',
                'id': 3,
                'subchild3': {
                    'subsubchild1': 'value5',
                    'subsubchild2': 'value5',
                    'subsubchild3': [{
                        'subsubchild4': 'value11',
                        'subsubchild5': 'value12'
                    }]
                }
            },
            {
                'subchild2': 'value3',
                'subchild1': 'value',
                'type': 'value2',
                'id': 2,
                'subchild3': {
                    'subsubchild1': 'value6',
                    'subsubchild2': 'value6',
                    'subsubchild3': [{
                        'subsubchild4': 'value10',
                        'subsubchild5': 'value13'
                    }]
                }
            },
        ]},
        {'child3': [
            {
                'subchild1': 'value',
                'subchild2': 'value1',
                'type': 'value1',
                'id': 1,
                'subchild3': {
                    'subsubchild1': 'value4',
                    'subsubchild2': 'value4',
                    'subsubchild3': [{
                        'subsubchild4': 'value10',
                        'subsubchild5': 'value11'
                    }]
                }

            },
            {
                'subchild1': 'value',
                'subchild2': 'value2',
                'type': 'value3',
                'id': 3,
                'subchild3': {
                    'subsubchild1': 'value5',
                    'subsubchild2': 'value5',
                    'subsubchild3': [{
                        'subsubchild4': 'value11',
                        'subsubchild5': 'value12'
                    }]
                }
            },
            {
                'subchild2': 'value3',
                'subchild1': 'value',
                'type': 'value2',
                'id': 2,
                'subchild3': {
                    'subsubchild1': 'value6',
                    'subsubchild2': 'value6',
                    'subsubchild3': [{
                        'subsubchild4': 'value10',
                        'subsubchild5': 'value13'
                    }]
                }
            }]
        }]}})


@pytest.fixture()
def pdict_object():
    obj = dict(
        a=1,
        b=2,
        c=['c1', 'c2'],
        d=dict(nested=[1, dict(foo='bar', baz={})]),
        Prices=[{"Value": 200.92, "Currency": "EUR", "Type": "NET"},
                {"Value": 190.0, "Currency": "EUR", "Type": "GROSS"}]
    )
    return PathDict(obj)


def test_pathdict_init():
    with pytest.raises(TypeError):
        PathDict((1, 2, 3))

    with pytest.raises(TypeError):
        PathDict([1, 2, 3])

    assert PathDict([('a', 1), ('b', 2), ('c', 3)]).to_dict() == {'a': 1, 'b': 2, 'c': 3}


def test_pathitems(pdict_object: PathDict):
    od = OrderedDict()  # type: OrderedDict
    od['a'] = 1
    od['b'] = 2
    od['c'] = 3

    pd = PathDict(od)

    it = pd.pathitems()

    assert next(it)() == (('a',), 1)
    assert next(it)() == (('b',), 2)
    assert next(it)() == (('c',), 3)

    with pytest.raises(StopIteration):
        next(it)

    it = pd.pathitems()

    assert next(it)() == (('a',), 1)
    assert next(it)() == (('b',), 2)
    assert next(it)() == (('c',), 3)

    with pytest.raises(StopIteration):
        next(it)


def test_get(pdict_object: PathDict):
    assert pdict_object.get(['a']) == 1
    assert pdict_object.get('a') == 1
    assert pdict_object.get(('a',)) == 1
    assert pdict_object.get(['d', 'nested', 1, 'foo']) == 'bar'
    assert pdict_object.get('d.nested.1.foo') == 'bar'

    with pytest.raises(PathNotExistsError):
        pdict_object.get('d.nested.1.too-bad', raise_error=True)

    with pytest.raises(PathNotExistsError):
        pdict_object.get('d.nested.1.too-bad')
        pdict_object.raise_for_get()

    assert pdict_object.get('d.nested.1.too-bad', 'x') == 'x'
    assert pdict_object.get('d').get('nested')[1].to_dict() == dict(foo='bar', baz={})
    assert pdict_object.d.nested[1]['foo'] == 'bar'
    assert pdict_object.d.nested[1].foo == 'bar'

    with pytest.raises(AttributeError):
        assert pdict_object.d.nested[1].foo.bad


def test_set(pdict_object: PathDict):
    pdict_object.set('a', 2)
    assert pdict_object.get('a') == 2

    pdict_object.set(['d', 'nested', 0], 'a')

    assert pdict_object.get(['d', 'nested', 0]) == 'a'
    pdict_object.set(['d', 'nested', 1], 'b')

    assert pdict_object.get(['d', 'nested', 1]) == 'b'
    assert len(pdict_object.get(['d', 'nested'])) == 2

    pdict_object.set('d.nested.1.baz', {'a': 11})
    assert pdict_object.get('d.nested.1.baz.a') == 11

    pdict_object.set('d.0', {'a': 'aa', 'b': [1, 2, 3]})
    assert pdict_object.get('d') == [{'a': 'aa', 'b': [1, 2, 3]}]

    pdict_object.set('d.3', 'no longer empty')
    assert pdict_object.get('d') == [{'a': 'aa', 'b': [1, 2, 3]}, None, None, 'no longer empty']

    with pytest.raises(PathNotExistsError):
        assert pdict_object.set('d.6', 'too-bad', dynamic=False)

    pdict_object.set(('Prices', 0, 'Value'), {"__CHANGE__": [200.92, 250.92]})
    assert pdict_object.get(('Prices', 0, 'Value')) == {"__CHANGE__": [200.92, 250.92]}

    pdict_object.set(('r', 1, 'rr', 2, 'zz'), {'n': {'a': 1}})
    assert pdict_object.to_dict()['r'] == [None, {'rr': [None, None, {'zz': {'n': {'a': 1}}}]}]

    pdict = PathDict()
    pdict.set(('a', 'b', 0), {'aaa': {'ff': 55}})
    assert pdict.to_dict() == {'a': {'b': [{'aaa': {'ff': 55}}]}}

    pdict.a.b = 33
    assert pdict.a.b == 33

    obj = dict(d=dict(nested=[1, dict(foo='bar', baz={})]))
    pdict = PathDict(obj)
    pdict.set(('d', 'nested'), 'foo')
    assert pdict.to_dict() == {'d': {'nested': 'foo'}}

    pdict = PathDict()
    pdict.set(('Root',), {'key': 'value'})
    assert pdict.to_dict() == dict(Root={'key': 'value'})

    pdict = PathDict()
    pdict.set('Root.Child', 123456)
    assert pdict.to_dict() == dict(Root={'Child': 123456})
    object_a = {
        "id": 1,
        "state": "open",
        "title": "Found a bug",
        "user": {
            "login": "octocat",
            "id": 1,
        },
        "labels": [
            {
                "id": 208045946,
                "name": "bug"
            }
        ],
        "milestone": {
            "id": 1002604,
            "state": "open",
            "title": "v1.0",
            "creator": {
                "login": "octocat",
                "id": 1,
            },
            "open_issues": 4,
            "closed_issues": 8,
        }
    }
    pd = PathDict(object_a)
    pd.set(('milestone', 'creator', 'login'), 'ddd')


def test_search_with_condition():
    obj = PathDict(a=[{'d': 3}, {'e': 4}, {'f': 5}], b=[1, 2, 3], c={'d': 3, 'e': 4, 'f': 5})

    assert obj.search('a', condition=lambda p, v: any(k in ['d', 'f'] for k in v.keys())) == \
           [PathDict([('d', 3)]), PathDict([('f', 5)])]

    assert obj.search('b', condition=lambda p, v: v == 3, rpath=True) == [PathDictItem('b.2', 3)]
    assert obj.search('c', condition=lambda p, v: p == 'd' and v == 3, rpath=True) == [PathDictItem('c.d', 3)]

    assert obj.search('a.*', condition=lambda p, v: p in ['d', 'f'], rpath=True) == \
           [PathDictItem('a.0.d', 3), PathDictItem('a.2.f', 5)]

    assert obj.search('b.*', condition=lambda v: v in [3, 1], rpath=True) == \
           [PathDictItem('b.0', 1), PathDictItem('b.2', 3)]

    assert obj.search('c.*', condition=lambda v: v in [3, 5], rpath=True) == \
           [PathDictItem('c.d', 3), PathDictItem('c.f', 5)]


def test_append_dict(pdict_object: PathDict):
    pdict_object.append('d', PathDict({'n': True}))
    assert pdict_object.get('d').to_dict() == {'n': True, 'nested': [1, {'baz': {}, 'foo': 'bar'}]}


def test_raise_for_get(pdict_object: PathDict):
    assert not pdict_object.raise_for_get()


def test_comparison(pdict_object: PathDict):
    obj = dict(
        a=1,
        b=2,
        c=['c1', 'c2'],
        d=dict(nested=[1, dict(foo='bar', baz={})]),
        Prices=[{"Value": 200.92, "Currency": "EUR", "Type": "NET"},
                {"Value": 190.0, "Currency": "EUR", "Type": "GROSS"}]
    )
    o = PathDict(dict(nested=[1, dict(foo='bar', baz={})]))
    assert pdict_object == PathDict(obj)
    assert pdict_object != o
    assert pdict_object.d.nested[1] == PathDict(dict(foo='bar', baz={}))

    assert pdict_object.d.nested[1] != dict(foo='bar', baz={'a': 1})

    assert pdict_object.d.nested[1] == dict(foo='bar', baz={})
    assert 'a' in pdict_object

    a = {"Foo": True, "Bar": "CC", "Baz": "VI"}
    b = {"Foo": False, "Bar": "CC", "Baz": "VI"}
    assert PathDict(a) != PathDict(b)

    a = {"Foo": True, "Bar": "CC", "Baz": "VI"}
    b = {"Foo": True, "Bar": "CC", "Baz": "VI"}
    assert PathDict(a) == PathDict(b)


def test_del(pdict_object: PathDict):
    assert pdict_object.a == 1

    del pdict_object.a

    with pytest.raises(KeyError):
        assert pdict_object.a

    assert len(pdict_object.c) == 2
    del pdict_object.c[0]

    assert len(pdict_object.c) == 1
    assert pdict_object.c[0] == 'c2'

    assert pdict_object.Prices[0].Value == 200.92
    del pdict_object.Prices[0].Value

    with pytest.raises(KeyError):
        assert pdict_object.Prices[0].Value
        pdict_object.raise_for_get()

    assert pdict_object.Prices[0] == {"Currency": "EUR", "Type": "NET"}
    del pdict_object.Prices[1]

    assert len(pdict_object.Prices) == 1
    del pdict_object.Prices[0]

    assert len(pdict_object.Prices) == 0

    for o in pdict_object.d.nested[:]:
        pdict_object.d.nested.remove(o)

    assert len(pdict_object.d.nested) == 0

    pdict_a = PathDict(dict(Item=[{"a": 1, "Type": "A"}, {"b": 2, "Type": "B"}, {"c": 3, "Type": "C"}]))
    pdict_b = PathDict(dict(Item=[{"a": 1, "Type": "A"}, {"b": 2, "Type": "B"}, {"d": 4, "Type": "D"}]))

    lst_a = pdict_a.get('Item', [])  # type: list
    lst_b = pdict_b.get('Item', [])  # type: list

    for ia in lst_a[:]:
        for ib in lst_b[:]:
            if ia == ib:
                lst_a.remove(ia)
                lst_b.remove(ib)

    assert len(lst_a) == 1
    assert lst_a[0].c == 3
    assert lst_a[0].Type == 'C'

    assert len(lst_b) == 1
    assert lst_b[0].d == 4
    assert lst_b[0].Type == 'D'

    pdict = PathDict(dict(nested=[1, dict(foo='bar', baz={})]))
    pdict.delete(('nested', 1))
    assert pdict.to_dict() == dict(nested=[1])

    with pytest.raises(PathNotExistsError):
        pdict.delete(('nested', 6))
        assert pdict.to_dict() == dict(nested=[1])


def test_pop(pdict_object: PathDict):
    assert pdict_object.pop(('d', 'nested')) == [1, dict(foo='bar', baz={})]
    assert pdict_object == dict(a=1, b=2, c=['c1', 'c2'], d=dict(),
                                Prices=[{"Value": 200.92, "Currency": "EUR", "Type": "NET"},
                                        {"Value": 190.0, "Currency": "EUR", "Type": "GROSS"}])

    obj = PathDict(dict(a=1, b=2))
    assert obj.pop('a') == 1
    assert obj == dict(b=2)

    obj = PathDict(dict(d=dict(nested=[1, dict(foo='bar', baz={})])))
    assert obj.pop('d.nested.1.baz') == {}
    assert obj == dict(d=dict(nested=[1, dict(foo='bar')]))

    obj = PathDict(dict(d=dict(nested=[1, dict(foo='bar', baz={})])))
    assert obj.pop('d.nested.1.baz') == {}
    assert obj.pop('d.nested.0') == 1
    assert obj == dict(d=dict(nested=[dict(foo='bar')]))

    with pytest.raises(PathNotExistsError):
        obj.pop('bad-path')

    res = obj.pop('bad-path', 'default')
    assert res == 'default'


def test_path(pdict_object: PathDict):

    assert pdict_object.get(('Prices', 0), rpath=True).path.isequal(('Prices', 0))

    obj = pdict_object.get(('Prices', 0, 'Value'), rpath=True)
    assert obj.path.islast((0, 'Value'))
    assert obj.value == 200.92

    obj = pdict_object.get(('Prices', 1, 'Value'), rpath=True)
    assert obj.path.isfirst(('Prices', 1, 'Value'))
    assert obj.value == 190.0


def test_copy(pdict_object: PathDict):
    from copy import (copy, deepcopy)

    c = pdict_object
    assert c is pdict_object
    assert c == pdict_object

    c = pdict_object
    assert c.copy() is not pdict_object
    assert c == pdict_object

    c = copy(pdict_object)
    assert isinstance(c, PathDict)
    assert c == pdict_object
    assert c is not pdict_object

    c = deepcopy(pdict_object)
    assert isinstance(c, PathDict)
    assert c == pdict_object
    assert c is not pdict_object


def test_filter():
    assert PathDict(a=1, b=2, c=3).filter().to_dict() == {'a': 1, 'b': 2, 'c': 3}

    with pytest.raises(ValueError):
        PathDict(a=1, b=2, c=3).filter('E', 'D')

    d = {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                             {'__CHANGE__': [{'key': 'value', 'type': '2'},
                                             {'key': 'value1', 'type': '2'}]},
                             {'__DELETE__': {'key': 'value', 'type': '3'}},
                             {'__IGNORE__': {'key': 'value', 'type': '4'}},
                             {'__NEW__': {'key': 'value', 'type': '4'}}]}}

    pd = PathDict(d)

    assert pd.filter(ct.left).to_dict() == {'root': {'parent': [{'key': 'value', 'type': '1'},
                                                                {'key': 'value', 'type': '2'},
                                                                {'key': 'value', 'type': '3'}]}}

    assert pd.filter(ct.LEFT).to_dict() == {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                                                {'__CHANGE__': {'key': 'value', 'type': '2'}},
                                                                {'__DELETE__': {'key': 'value', 'type': '3'}}]}}

    assert pd.filter(ct.right).to_dict() == {'root': {'parent': [{'key': 'value', 'type': '1'},
                                                                 {'key': 'value1', 'type': '2'},
                                                                 {'key': 'value', 'type': '4'},
                                                                 {'key': 'value', 'type': '4'}]}}

    assert pd.filter(ct.RIGHT).to_dict() == {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                                                 {'__CHANGE__': {'key': 'value1', 'type': '2'}},
                                                                 {'__IGNORE__': {'key': 'value', 'type': '4'}},
                                                                 {'__NEW__': {'key': 'value', 'type': '4'}}]}}

    assert pd.filter(ct.EQUAL | ct.CHANGE | ct.NEW | ct.IGNORE | ct.REQUIRED).to_dict() == \
           {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                {'__CHANGE__': [{'key': 'value', 'type': '2'},
                                                {'key': 'value1', 'type': '2'}]},
                                {'__IGNORE__': {'key': 'value', 'type': '4'}},
                                {'__NEW__': {'key': 'value', 'type': '4'}}]}}

    assert pd.filter(['E', 'C', 'N', 'I', 'R']).to_dict() == \
           {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                {'__CHANGE__': [{'key': 'value', 'type': '2'},
                                                {'key': 'value1', 'type': '2'}]},
                                {'__IGNORE__': {'key': 'value', 'type': '4'}},
                                {'__NEW__': {'key': 'value', 'type': '4'}}]}}

    assert pd.filter('ECNIR').to_dict() == \
           {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                {'__CHANGE__': [{'key': 'value', 'type': '2'},
                                                {'key': 'value1', 'type': '2'}]},
                                {'__IGNORE__': {'key': 'value', 'type': '4'}},
                                {'__NEW__': {'key': 'value', 'type': '4'}}]}}

    assert pd.filter(['E', 'LC', 'N', 'I', 'R']).to_dict() == \
           {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                {'__CHANGE__': {'key': 'value', 'type': '2'}},
                                {'__IGNORE__': {'key': 'value', 'type': '4'}},
                                {'__NEW__': {'key': 'value', 'type': '4'}}]}}

    assert pd.filter(['E', 'RC', 'N', 'I', 'R']).to_dict() == \
           {'root': {'parent': [{'__EQUAL__': {'key': 'value', 'type': '1'}},
                                {'__CHANGE__': {'key': 'value1', 'type': '2'}},
                                {'__IGNORE__': {'key': 'value', 'type': '4'}},
                                {'__NEW__': {'key': 'value', 'type': '4'}}]}}


def test_deephash_set():
    assert deephash({1, 2, 3}) == deephash({3, 2, 1})


def test_set_comparison_type_by_conditions(complex_object: PathDict):
    obj = complex_object.copy()

    conditions = [
        ('root.parent2.*.child1', [1, 2, 3]),
        ('root.parent2.*.child0', None),
        ('root.parent2.*.child2', [('type', ['value2', 'value1']), ('subchild2', ['value2', 'value1'])]),
        ('root.parent2.*.child3.*.subchild3.subsubchild3.*.subsubchild4', ['value11']),
        ('root.parent2.*.child3.*.subchild3.subsubchild2', ['value5']),
        ('root.parent2.*.child3.*.subchild2', lambda val: val[-1].isdigit() and int(val[-1]) <= 3)
    ]

    obj.set_comparison_type_by_conditions(ct.IGNORE, conditions)

    assert obj.to_dict() == {'root': {'parent2': [
        {'child0': {'__IGNORE__': []}},
        {'child1': {'__IGNORE__': [1, 2, 3]}},
        {'child2': [{'__IGNORE__': {'id': 1,
                                    'subchild1': 'value',
                                    'subchild2': 'value1',
                                    'subchild3': {'subsubchild1': 'value4',
                                                  'subsubchild2': 'value4',
                                                  'subsubchild3': [{
                                                      'subsubchild4': 'value10',
                                                      'subsubchild5': 'value11'}]},
                                    'type': 'value1'}},
                    {'id': 3,
                     'subchild1': 'value',
                     'subchild2': 'value2',
                     'subchild3': {'subsubchild1': 'value5',
                                   'subsubchild2': 'value5',
                                   'subsubchild3': [
                                       {'subsubchild4': 'value11',
                                        'subsubchild5': 'value12'}]},
                     'type': 'value3'},
                    {'id': 2,
                     'subchild1': 'value',
                     'subchild2': 'value3',
                     'subchild3': {'subsubchild1': 'value6',
                                   'subsubchild2': 'value6',
                                   'subsubchild3': [
                                       {'subsubchild4': 'value10',
                                        'subsubchild5': 'value13'}]},
                     'type': 'value2'}]},
        {'child3': [{'id': 1,
                     'subchild1': 'value',
                     'subchild2': {'__IGNORE__': 'value1'},
                     'subchild3': {'subsubchild1': 'value4',
                                   'subsubchild2': 'value4',
                                   'subsubchild3': [
                                       {'subsubchild4': 'value10',
                                        'subsubchild5': 'value11'}]},
                     'type': 'value1'},
                    {'id': 3,
                     'subchild1': 'value',
                     'subchild2': {'__IGNORE__': 'value2'},
                     'subchild3': {'subsubchild1': 'value5',
                                   'subsubchild2': {'__IGNORE__': 'value5'},
                                   'subsubchild3': [{'subsubchild4': {
                                       '__IGNORE__': 'value11'},
                                       'subsubchild5': 'value12'}]},
                     'type': 'value3'},
                    {'id': 2,
                     'subchild1': 'value',
                     'subchild2': {'__IGNORE__': 'value3'},
                     'subchild3': {'subsubchild1': 'value6',
                                   'subsubchild2': 'value6',
                                   'subsubchild3': [
                                       {'subsubchild4': 'value10',
                                        'subsubchild5': 'value13'}]},
                     'type': 'value2'}]}]}}


def test_set_comparison_type_by_conditions2(complex_object: PathDict):
    obj = complex_object.copy()
    conditions = [
        ('root.parent2.*.*', [('type', ['value2'])]),
    ]

    obj.set_comparison_type_by_conditions(ct.IGNORE, conditions)

    assert obj.to_dict() == {'root': {'parent2': [{'child0': []},
                                                  {'child1': [1, 2, 3]},
                                                  {'child2': [{'id': 1,
                                                               'subchild1': 'value',
                                                               'subchild2': 'value1',
                                                               'subchild3': {'subsubchild1': 'value4',
                                                                             'subsubchild2': 'value4',
                                                                             'subsubchild3': [
                                                                                 {'subsubchild4': 'value10',
                                                                                  'subsubchild5': 'value11'}]},
                                                               'type': 'value1'},
                                                              {'id': 3,
                                                               'subchild1': 'value',
                                                               'subchild2': 'value2',
                                                               'subchild3': {'subsubchild1': 'value5',
                                                                             'subsubchild2': 'value5',
                                                                             'subsubchild3': [
                                                                                 {'subsubchild4': 'value11',
                                                                                  'subsubchild5': 'value12'}]},
                                                               'type': 'value3'},
                                                              {'__IGNORE__': {'id': 2,
                                                                              'subchild1': 'value',
                                                                              'subchild2': 'value3',
                                                                              'subchild3': {
                                                                                  'subsubchild1': 'value6',
                                                                                  'subsubchild2': 'value6',
                                                                                  'subsubchild3': [
                                                                                      {'subsubchild4': 'value10',
                                                                                       'subsubchild5': 'value13'}]},
                                                                              'type': 'value2'}}]},
                                                  {'child3': [
                                                      {'id': 1,
                                                       'subchild1': 'value',
                                                       'subchild2': 'value1',
                                                       'subchild3': {'subsubchild1': 'value4',
                                                                     'subsubchild2': 'value4',
                                                                     'subsubchild3': [{'subsubchild4': 'value10',
                                                                                       'subsubchild5': 'value11'}]},
                                                               'type': 'value1'},
                                                              {'id': 3,
                                                               'subchild1': 'value',
                                                               'subchild2': 'value2',
                                                               'subchild3': {'subsubchild1': 'value5',
                                                                             'subsubchild2': 'value5',
                                                                             'subsubchild3': [{
                                                                                 'subsubchild4': 'value11',
                                                                                 'subsubchild5': 'value12'}]},
                                                               'type': 'value3'},
                                                              {'__IGNORE__': {
                                                                  'id': 2,
                                                                  'subchild1': 'value',
                                                                  'subchild2': 'value3',
                                                                  'subchild3': {
                                                                      'subsubchild1': 'value6',
                                                                      'subsubchild2': 'value6',
                                                                      'subsubchild3': [{'subsubchild4': 'value10',
                                                                                        'subsubchild5': 'value13'}]},
                                                                  'type': 'value2'}}]}]}}
