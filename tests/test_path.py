import pytest  # type: ignore
from pathdict import (Path, PathDict, ComparisonType as ct, RequiredPathError, DuplicateComparisonTypeError)  # type: ignore
from pprint import pprint


@pytest.fixture()
def path_object():
    return Path(*('a', 0, 'c'))


def test_str(path_object: Path):
    assert str(path_object) == 'a.0.c'


def test_first(path_object: Path):
    assert path_object.first == ('a',)


def test_isequal(path_object: Path):
    assert path_object.isequal(('a', 0, 'c'))


def test_iscontains(path_object: Path):
    assert path_object.iscontains('0')
    assert path_object.iscontains((0,))


def test_issubpath(path_object: Path):
    assert Path(*('a', 0)).issubpath(path_object)
