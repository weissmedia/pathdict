import os
from setuptools import setup

this = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(this, 'pathdict', '__version__.py')) as f:
    exec(f.read(), about)

test_requirements = ['pytest', 'pytest-cov', 'pytest-mypy']

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    author=about['__author__'],
    author_email=about['__author_email__'],
    url=about['__url__'],
    packages=['pathdict'],
    package_dir={'pathdict': 'pathdict'},
    include_package_data=True,
    license=about['__license__'],
    zip_safe=True,
    classifiers=(
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6'
        'Topic :: Software Development :: Libraries :: Python Modules'
    ),
    tests_require=test_requirements
)
